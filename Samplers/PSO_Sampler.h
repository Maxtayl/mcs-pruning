/*
 * PSO_Pruner.h
 *
 *  Created on: Feb 25, 2011
 *      Author: rgreen
 */

#ifndef PSO_SAMPLER_H_
#define PSO_SAMPLER_H_

#include "Sampler.h"
#include "binaryParticle.h"

class PSO_Sampler : public Sampler {
	public:
		PSO_Sampler(int _Np, int _Nd, int _Nt);
		virtual ~PSO_Sampler();
		virtual void run(MTRand& mt);
		bool checkConvergence(int j);

	protected:
		void 	initPopulation(MTRand& mt);
		void 	evaluateFitness();
		void 	updatePositions(MTRand& mt);
		void 	clearVectors();
		double 	sigMoid(double v);

		int totalIterations;
		double sumX, sumXSquared;
		double nSumX, nSumXSquared;
		double sigma;
		long double epsilon;

		int Np, Nd, Nt;
		double C1, C2, W;
		double gBestValue;

		double change, totalChange;

		std::vector<binaryParticle> swarm;
		std::vector<int> gBest;
};

#endif /* PSO_SAMPLER_H_ */
