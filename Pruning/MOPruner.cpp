
/*
 * MOPruner.cpp
 *
 *  Created on: Nov 12, 2010
 *      Author: rgreen
 */

#include "MOPruner.h"
#include <iostream>

using namespace std;

MOPruner::MOPruner(int np, int nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul)
        : Pruner(np, nt, g, l, opf, pload,  ul){
}

void MOPruner::Prune(MTRand& mt){
    Pruner::Prune(mt);
}
void MOPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);
}
std::vector<double> MOPruner::EvaluateSolution(std::vector<int> curSolution){

    std::string s = "";
    for(unsigned int i=0; i< curSolution.size(); i++){
        if(curSolution[i] == 0){
            s += "0";
        }else{
            s += "1";
        }
    }

    return EvaluateSolution(s);
}
std::vector<double> MOPruner::EvaluateSolution(std::string curSolution){
    std::vector<double> retValues;

    CStopWatch timer;

    int		totalUp 		= 0,	// Generators up in a single group
            totalDown 		= 0,	// Total Generators in a single group
            tGens 			= 1,
            tUp 			= 1,
            curBus			= 0,
            nextBus			= 0;
    double 	stateProb 		= 1.0,  // Probability for Calculations
            curtailment     = 0.0,
            excess,
            totalCapacity	= 0.00,
            copy			= 1.00,
            retValue;
    bool 	failed 			= false;
    MTRand mt;
    std::vector<double> vCurSolution;
    std::vector<double> phevLoad, phevGen;
    std::vector<double> vSolution(gens.size() + lines.size() + 1);
    std::string c;

    if(useLines){ vSolution.resize(gens.size() + lines.size() + 1, 0);}
    else		{ vSolution.resize(gens.size() + 1, 0);}

    classifier->reset();
    for(unsigned int i=0; i< gens.size(); i++){
        nextBus = gens[i].getBus();
        if(nextBus != curBus){
            curBus = nextBus;
            copy *= Utils::combination(tGens, tUp);
            tUp = 0; tGens = 0;
        }
        tGens++;

        if(curSolution[i] == '1'){
            stateProb 	*= (1-gens[i].getOutageRate());
            totalCapacity += gens[i].getPG()/100;
            vSolution[i] = 1;
            tUp++; totalUp++;
        }else{
            stateProb 	*=  gens[i].getOutageRate();
            vSolution[i] = 0;
            totalDown++;
        }
    }

    if(useLines && lines.size() > 0){
        for(unsigned int i=0; i<lines.size(); i++){
            if(curSolution[gens.size() + i] == '1'){
                stateProb *= (1-(lines[i].getOutageRate()*lineAdjustment));
                vSolution[gens.size() + i] = 1;
            }else{
                stateProb *=  (lines[i].getOutageRate()*lineAdjustment);
                vSolution[gens.size() + i] = 1;
            }
        }
    }

    timer.startTimer();
    if(successStates.find(curSolution) != successStates.end() || failureStates.find(curSolution) != failureStates.end()){
        collisions++;
        retValues.resize(4, 0);
        return retValues;
    }
    timer.stopTimer();
    searchTime += timer.getElapsedTime();

    timer.startTimer();

    if(usePHEVs){
        Utils::calculatePHEVLoad(penetrationLevel,rho, totalVehicles, numBuses, phevLoad, phevGen, mt, phevPlacement);
        classifier->addLoad(phevLoad);
    }
    if(useLocalSearch && sampledStates.find(curSolution) != sampledStates.end()){
        if(sampledStates[curSolution] == 0){
            failed = true;
        }else{
            failed = false;
        }
    }else{
        curtailment = classifier->run(curSolution,excess);
        if(curtailment != 0.0){ failed = true;}
        else                  { failed = false;
        }
    }
    timer.stopTimer();
    oTime += timer.getElapsedTime();

    if(failed){
        failureStates.insert(std::pair<std::string, double>(curSolution, stateProb));
        sampledStates.insert(std::pair<std::string, int>(curSolution, 0));
    }else{
        successStates.insert(std::pair<std::string, double>(curSolution, stateProb));
        sampledStates.insert(std::pair<std::string, int>(curSolution, 1));
    }
    copy *= stateProb;

    retValues.push_back(stateProb);
    retValues.push_back(-curtailment);
    retValues.push_back(excess);
    retValues.push_back(copy);
    //retValues.push_back(stateProb*copy);
    if(negateFitness){
        for(unsigned int i=0; i<retValues.size(); i++){
            retValues[i] *= -1;
        }
    }

    return retValues;

}

