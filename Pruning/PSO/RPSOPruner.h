/*
 * RPSOPruner.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef RPSOPRUNER_H_
#define RPSOPRUNER_H_

#include <vector>
#include <map>

#include "Pruner.h"
#include "binaryParticle.h"

class RPSOPruner : public Pruner {
	public:
		RPSOPruner(int popSize, int generations, double c1, double c2, double w, Classifier* lp, std::vector<Generator> g, std::vector<Line> l,
				double pLoad, bool ul=false);
		RPSOPruner(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
		void Init(int popSize, int generations, double c1, double c2, double w, Classifier* lp, std::vector<Generator> g, std::vector<Line> l,
				double pLoad, bool ul=false);
		void Reset(int np, int nt);
		virtual ~RPSOPruner();

		void Prune(MTRand& mt);

	protected:
		bool    isConverged();
		void 	initPopulation(MTRand& mt);
		void 	evaluateFitness();
		void 	updatePositions(MTRand& mt);
		void 	clearVectors();
		double 	sigMoid(double v);

		int iterations;

		double gBestValue;
		double C1;
		double C2;
		double W;
		bool useLP;
		double A, B, C;
		Classifier* lp;
		std::vector<binaryParticle> swarm;
		std::vector<int> gBest;

		// Stat Collection
		double pruningTime;

};

#endif /* PSOPRUNER_H_ */
